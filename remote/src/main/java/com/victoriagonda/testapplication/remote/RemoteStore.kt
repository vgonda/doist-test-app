package com.victoriagonda.testapplication.remote

import android.content.Context
import com.google.gson.Gson
import com.victoriagonda.testapplication.remote.models.MessagesResponse
import io.reactivex.Single
import java.nio.charset.Charset

class RemoteStore(private val context: Context, private val gson: Gson) {

    fun getMessagesResponse(): Single<MessagesResponse> {
        return Single.defer {
            Single.just(gson.fromJson(getAssetJsonData(), MessagesResponse::class.java))
        }
    }

    private fun getAssetJsonData(): String {
        val inputStream = context.assets.open("data.json")
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        return String(buffer, Charset.forName("UTF-8"))
    }
}