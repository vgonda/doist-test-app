package com.victoriagonda.testapplication.remote.models

data class MessageModel(
    val id: Int,
    val userId: Int,
    val content: String,
    val attachments: List<AttachmentModel>?,
    val position: Int
)