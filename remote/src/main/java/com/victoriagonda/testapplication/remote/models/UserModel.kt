package com.victoriagonda.testapplication.remote.models

data class UserModel(
    val id: Int,
    val name: String,
    val avatarId: String
)