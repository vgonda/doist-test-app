package com.victoriagonda.testapplication.remote.models

data class MessagesResponse (
    val messages : List<MessageModel>,
    val users : List<UserModel>
)