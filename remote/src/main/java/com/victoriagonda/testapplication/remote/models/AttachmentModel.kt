package com.victoriagonda.testapplication.remote.models

data class AttachmentModel(
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)