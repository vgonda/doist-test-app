package com.victoriagonda.testapplication.data.interactors

import com.victoriagonda.testapplication.data.MessagesRepository
import com.victoriagonda.testapplication.data.models.Attachment
import com.victoriagonda.testapplication.data.models.Message
import io.reactivex.Completable
import io.reactivex.Scheduler

class DeleteAttachmentFromMessage(
    private val messagesRepository: MessagesRepository,
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) {

    fun execute(message: Message, attachment: Attachment): Completable {
        val newMessage = message.copy(attachments = message.attachments?.minus(attachment))
        return messagesRepository.updateMessage(newMessage)
            .subscribeOn(executionScheduler)
            .observeOn(postExecutionScheduler)
    }
}