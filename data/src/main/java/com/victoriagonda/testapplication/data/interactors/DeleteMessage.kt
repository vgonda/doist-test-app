package com.victoriagonda.testapplication.data.interactors

import com.victoriagonda.testapplication.data.MessagesRepository
import com.victoriagonda.testapplication.data.models.Message
import io.reactivex.Completable
import io.reactivex.Scheduler

class DeleteMessage(
    private val messagesRepository: MessagesRepository,
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) {
    fun execute(message: Message): Completable {
        return messagesRepository.deleteMessage(message)
            .subscribeOn(executionScheduler)
            .observeOn(postExecutionScheduler)
    }
}