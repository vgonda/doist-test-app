package com.victoriagonda.testapplication.data.models

data class Message(
    val id: Int,
    val userId: Int,
    val content: String,
    val attachments: List<Attachment>?,
    var user: User,
    val position: Int
)