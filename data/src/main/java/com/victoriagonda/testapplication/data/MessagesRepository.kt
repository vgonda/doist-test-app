package com.victoriagonda.testapplication.data

import com.victoriagonda.testapplication.cache.CacheStore
import com.victoriagonda.testapplication.data.mapper.MessageCacheMapper
import com.victoriagonda.testapplication.data.models.Message
import com.victoriagonda.testapplication.remote.RemoteStore
import io.reactivex.Completable

class MessagesRepository(
    private val remoteStore: RemoteStore,
    private val cacheStore: CacheStore,
    private val messageMapper: MessageCacheMapper
) {

    fun fetchMessages(): Completable {
        return cacheStore.messagesSaved()
            .flatMapCompletable { saved ->
                if (!saved) {
                    remoteStore.getMessagesResponse().flatMapCompletable { response ->
                        val users = response.users
                        val messages = response.messages.map { message ->
                            messageMapper.mapToCache(message, users.find { it.id == message.userId }!! )
                        }
                        cacheStore.saveMessages(messages)
                    }
                } else {
                    Completable.complete()
                }
            }
    }

    fun getMessages(): androidx.paging.DataSource.Factory<Int, Message> {
        return cacheStore.getMessages().map { messageMapper.mapFromCache(it) }
    }

    fun deleteMessage(message: Message): Completable {
        return cacheStore.deleteMessage(messageMapper.mapToCache(message))
    }

    fun updateMessage(message: Message): Completable {
        return cacheStore.saveMessage(messageMapper.mapToCache(message))
    }
}