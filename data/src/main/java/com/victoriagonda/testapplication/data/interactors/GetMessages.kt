package com.victoriagonda.testapplication.data.interactors

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.victoriagonda.testapplication.data.MessagesRepository
import com.victoriagonda.testapplication.data.models.Message
import io.reactivex.Observable
import io.reactivex.Scheduler

class GetMessages(
    private val messagesRepository: MessagesRepository,
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) {

    fun execute(): Observable<PagedList<Message>> {
        return RxPagedListBuilder(messagesRepository.getMessages(), 20)
            .buildObservable()
            .subscribeOn(executionScheduler)
            .observeOn(postExecutionScheduler)
    }
}