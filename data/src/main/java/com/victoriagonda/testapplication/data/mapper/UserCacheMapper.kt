package com.victoriagonda.testapplication.data.mapper

import com.victoriagonda.testapplication.cache.models.CachedUser
import com.victoriagonda.testapplication.data.models.User
import com.victoriagonda.testapplication.remote.models.UserModel

class UserCacheMapper {

    fun mapToCache(user: UserModel): CachedUser {
        return CachedUser(user.id, user.name, user.avatarId)
    }

    fun mapToCache(user: User): CachedUser {
        return CachedUser(user.id, user.name, user.avatarId)
    }

    fun mapFromCache(user: CachedUser): User {
        return User(user.id, user.name, user.avatarId)
    }
}
