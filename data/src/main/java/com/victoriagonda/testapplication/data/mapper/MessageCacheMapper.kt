package com.victoriagonda.testapplication.data.mapper

import com.victoriagonda.testapplication.cache.models.CachedMessage
import com.victoriagonda.testapplication.data.models.Message
import com.victoriagonda.testapplication.remote.models.MessageModel
import com.victoriagonda.testapplication.remote.models.UserModel

class MessageCacheMapper(
    private val attachmentMapper: AttachmentCacheMapper,
    private val userCacheMapper: UserCacheMapper
) {

    fun mapToCache(message: MessageModel, user: UserModel): CachedMessage {
        return CachedMessage(message.id, message.userId, message.content,
            message.attachments?.map { attachmentMapper.mapToCache(it) },
            userCacheMapper.mapToCache(user), message.position)
    }

    fun mapToCache(message: Message): CachedMessage {
        return CachedMessage(message.id, message.userId, message.content,
            message.attachments?.map { attachmentMapper.mapToCache(it) },
            userCacheMapper.mapToCache(message.user), message.position)
    }

    fun mapFromCache(message: CachedMessage): Message {
        return Message(message.id, message.userId, message.content,
            message.attachments?.map { attachmentMapper.mapFromCache(it) },
            userCacheMapper.mapFromCache(message.user), message.position)
    }
}
