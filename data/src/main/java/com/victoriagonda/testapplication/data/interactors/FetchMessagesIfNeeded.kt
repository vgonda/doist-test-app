package com.victoriagonda.testapplication.data.interactors

import com.victoriagonda.testapplication.data.MessagesRepository
import io.reactivex.Completable
import io.reactivex.Scheduler

class FetchMessagesIfNeeded(
    private val messagesRepository: MessagesRepository,
    private val executionScheduler: Scheduler,
    private val postExecutionScheduler: Scheduler
) {
    fun execute(): Completable {
        return messagesRepository.fetchMessages()
            .subscribeOn(executionScheduler)
            .observeOn(postExecutionScheduler)
    }
}