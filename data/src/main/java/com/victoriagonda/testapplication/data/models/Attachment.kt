package com.victoriagonda.testapplication.data.models

data class Attachment(
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
) {
    val httpsUrl: String
    get() {
        return if (thumbnailUrl.startsWith("https")) url
        else url.replace("http", "https")
    }
}