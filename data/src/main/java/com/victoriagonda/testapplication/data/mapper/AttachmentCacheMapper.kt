package com.victoriagonda.testapplication.data.mapper

import com.victoriagonda.testapplication.cache.models.CachedAttachment
import com.victoriagonda.testapplication.data.models.Attachment
import com.victoriagonda.testapplication.remote.models.AttachmentModel

class AttachmentCacheMapper {

    fun mapToCache(attachment: AttachmentModel): CachedAttachment {
        return CachedAttachment(attachment.id, attachment.title, attachment.url,
            attachment.thumbnailUrl)
    }

    fun mapToCache(attachment: Attachment): CachedAttachment {
        return CachedAttachment(attachment.id, attachment.title, attachment.url,
            attachment.thumbnailUrl)
    }

    fun mapFromCache(attachment: CachedAttachment): Attachment {
        return Attachment(attachment.id, attachment.title, attachment.url,
            attachment.thumbnailUrl)
    }
}
