package com.victoriagonda.testapplication.data.models

data class User(
    val id: Int,
    val name: String,
    val avatarId: String
)