package com.victoriagonda.testapplication.cache

import androidx.paging.DataSource
import com.victoriagonda.testapplication.cache.db.AppDatabase
import com.victoriagonda.testapplication.cache.models.CachedMessage
import io.reactivex.Completable
import io.reactivex.Single

class CacheStore(private val database: AppDatabase) {

    fun saveMessages(messages: List<CachedMessage>): Completable {
        return Completable.defer {
            database.messageDao().insertAll(*(messages.toTypedArray()))
            Completable.complete()
        }
    }

    fun getMessages(): DataSource.Factory<Int, CachedMessage> {
        return database.messageDao().getAll()
    }

    fun messagesSaved(): Single<Boolean> {
        return database.messageDao().getMessagesCount().map { it > 0 }
    }

    fun deleteMessage(message: CachedMessage): Completable {
        return Completable.defer {
            database.messageDao().deleteMessage(message)
            Completable.complete()
        }
    }

    fun saveMessage(message: CachedMessage): Completable {
        return Completable.defer {
            database.messageDao().insertAll(message)
            Completable.complete()
        }
    }
}