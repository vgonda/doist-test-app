package com.victoriagonda.testapplication.cache.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.victoriagonda.testapplication.cache.models.CachedMessage

@Database(entities = [CachedMessage::class], version = 1)
@TypeConverters(AttachmentTypeConverter::class, UserTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun messageDao(): MessageDao
}