package com.victoriagonda.testapplication.cache.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.victoriagonda.testapplication.cache.models.CachedUser

class UserTypeConverter {

    @TypeConverter
    fun fromUser(users: CachedUser): String {
        return Gson().toJson(users)
    }

    @TypeConverter
    fun toUser(users: String): CachedUser {
        return Gson().fromJson(users, CachedUser::class.java)
    }
}