package com.victoriagonda.testapplication.cache.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message")
data class CachedMessage(
    @PrimaryKey val id: Int,
    val userId: Int,
    val content: String,
    val attachments: List<CachedAttachment>?,
    val user: CachedUser,
    val position: Int
)