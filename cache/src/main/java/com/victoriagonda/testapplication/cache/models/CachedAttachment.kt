package com.victoriagonda.testapplication.cache.models

data class CachedAttachment(
    val id: String,
    val title: String,
    val url: String,
    val thumbnailUrl: String
)