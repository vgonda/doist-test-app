package com.victoriagonda.testapplication.cache.db

import androidx.paging.DataSource
import androidx.room.*
import com.victoriagonda.testapplication.cache.models.CachedMessage
import io.reactivex.Single

@Dao
interface MessageDao {

    @Query("SELECT * FROM message ORDER BY position ASC")
    fun getAll(): DataSource.Factory<Int, CachedMessage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg messages: CachedMessage)

    @Delete
    fun deleteMessage(message: CachedMessage)

    @Query("SELECT COUNT(*) FROM message")
    fun getMessagesCount(): Single<Int>
}