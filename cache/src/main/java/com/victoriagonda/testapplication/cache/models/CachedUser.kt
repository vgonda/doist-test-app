package com.victoriagonda.testapplication.cache.models

data class CachedUser(
    val id: Int,
    val name: String,
    val avatarId: String
)