package com.victoriagonda.testapplication.cache.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.victoriagonda.testapplication.cache.models.CachedAttachment

class AttachmentTypeConverter {

    @TypeConverter
    fun fromAttachmentList(attachments: List<CachedAttachment>?): String? {
        return attachments?.let { Gson().toJson(it) }
    }

    @TypeConverter
    fun toAttachmentList(attachments: String?): List<CachedAttachment>? {
        val listType = object : TypeToken<List<CachedAttachment>>() {}.type
        return attachments?.let { Gson().fromJson(it, listType) }
    }
}