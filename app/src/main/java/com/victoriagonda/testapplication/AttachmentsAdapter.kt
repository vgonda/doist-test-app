package com.victoriagonda.testapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.victoriagonda.testapplication.data.models.Attachment
import com.victoriagonda.testapplication.data.models.Message
import com.victoriagonda.testapplication.transformations.RoundedCornerTransformation
import kotlinx.android.synthetic.main.item_attachment.view.*

class AttachmentsAdapter(
    private val message: Message,
    private val deleteAttachmentListener: (Message, Attachment) -> Unit
) : RecyclerView.Adapter<AttachmentsAdapter.AttachmentViewHolder>() {

    init {
        setHasStableIds(true)
    }

    private val attachments = message.attachments ?: listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttachmentViewHolder {
        return AttachmentViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_attachment, parent, false))
    }

    override fun getItemCount(): Int {
        return attachments.size
    }

    override fun onBindViewHolder(holder: AttachmentViewHolder, position: Int) {
        holder.bind(attachments[position], message, deleteAttachmentListener)
    }

    override fun getItemId(position: Int): Long {
        return attachments[position].id.hashCode().toLong()
    }

    class AttachmentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(
            attachment: Attachment,
            message: Message,
            deleteAttachmentListener: (Message, Attachment) -> Unit
        ) {
            Picasso.get()
                .load(attachment.httpsUrl)
                .transform(RoundedCornerTransformation(4))
                .into(itemView.image_attachment)
            itemView.text_attachment.text = attachment.title
            itemView.setOnLongClickListener {
                deleteAttachmentListener(message, attachment)
                true
            }
        }
    }
}
