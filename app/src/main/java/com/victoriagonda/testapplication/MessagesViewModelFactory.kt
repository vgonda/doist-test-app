package com.victoriagonda.testapplication

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.victoriagonda.testapplication.data.interactors.DeleteAttachmentFromMessage
import com.victoriagonda.testapplication.data.interactors.DeleteMessage
import com.victoriagonda.testapplication.data.interactors.FetchMessagesIfNeeded
import com.victoriagonda.testapplication.data.interactors.GetMessages

class MessagesViewModelFactory(
    private val getMessages: GetMessages,
    private val deleteMessage: DeleteMessage,
    private val fetchMessagesIfNeeded: FetchMessagesIfNeeded,
    private val deleteAttachmentFromMessage: DeleteAttachmentFromMessage,
    private val application: Application
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return MessagesViewModel(
            getMessages,
            deleteMessage,
            fetchMessagesIfNeeded,
            deleteAttachmentFromMessage,
            application
        ) as T
    }
}