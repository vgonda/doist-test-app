package com.victoriagonda.testapplication.transformations

/*
From https://gist.github.com/aprock/6213395
 */

import android.graphics.*
import android.graphics.Bitmap.Config

// enables hardware accelerated rounded corners
// original idea here : http://www.curious-creature.org/2012/12/11/android-recipe-1-image-with-rounded-corners/

// radius is corner radii in dp
// margin is the board in dp
class RoundedCornerTransformation(
    private val radius: Int
) : com.squareup.picasso.Transformation {

    override fun transform(source: Bitmap): Bitmap {
        val paint = Paint()
        paint.isAntiAlias = true
        paint.shader = BitmapShader(source, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)

        val output = Bitmap.createBitmap(source.width, source.height, Config.ARGB_8888)
        val canvas = Canvas(output)
        canvas.drawRoundRect(
            RectF(
                0F,
                0F,
                (source.width).toFloat(),
                (source.height).toFloat()
            ), radius.toFloat(), radius.toFloat(), paint
        )

        if (source != output) {
            source.recycle()
        }

        return output
    }

    override fun key(): String {
        return "rounded(radius=$radius)"
    }
}