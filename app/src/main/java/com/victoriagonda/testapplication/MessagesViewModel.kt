package com.victoriagonda.testapplication

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.victoriagonda.testapplication.data.interactors.DeleteAttachmentFromMessage
import com.victoriagonda.testapplication.data.interactors.DeleteMessage
import com.victoriagonda.testapplication.data.interactors.FetchMessagesIfNeeded
import com.victoriagonda.testapplication.data.interactors.GetMessages
import com.victoriagonda.testapplication.data.models.Attachment
import com.victoriagonda.testapplication.data.models.Message
import io.reactivex.disposables.CompositeDisposable

class MessagesViewModel(
    private val getMessages: GetMessages,
    private val deleteMessage: DeleteMessage,
    private val fetchMessagesIfNeeded: FetchMessagesIfNeeded,
    private val deleteAttachmentFromMessage: DeleteAttachmentFromMessage,
    application: Application
) : AndroidViewModel(application) {

    private val messagesLiveData = MutableLiveData<PagedList<Message>>()
    private val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun fetchMessages() {
        fetchMessagesIfNeeded.execute()
            .andThen(getMessages.execute())
            .subscribe({
                messagesLiveData.postValue(it)
            }, {
                Log.e(this::class.java.simpleName, "Error getting messages", it)
            })
            .let(disposables::add)
    }

    fun observeMessages(): LiveData<PagedList<Message>> {
        return messagesLiveData
    }

    fun deleteMessage(message: Message) {
        deleteMessage.execute(message)
            .subscribe()
            .let(disposables::add)
    }

    fun deleteAttachment(message: Message, attachment: Attachment) {
        deleteAttachmentFromMessage.execute(message, attachment)
            .subscribe()
            .let(disposables::add)
    }
}