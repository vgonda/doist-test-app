package com.victoriagonda.testapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.victoriagonda.testapplication.data.models.Attachment
import com.victoriagonda.testapplication.data.models.Message
import com.victoriagonda.testapplication.transformations.CircleTransform
import kotlinx.android.synthetic.main.item_from.view.*

class MessagesAdapter : PagedListAdapter<Message, MessagesAdapter.MessageViewHolder>(diffCallback) {

    companion object {
        const val FROM_VIEW_TYPE = 0
        const val TO_VIEW_TYPE = 1

        private val diffCallback = object : DiffUtil.ItemCallback<Message>() {
            override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean =
                oldItem == newItem
        }
    }

    lateinit var onLongPress: (Message) -> Unit
    lateinit var deleteAttachmentListener: (Message, Attachment) -> Unit

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MessageViewHolder {
        return when (viewType) {
            FROM_VIEW_TYPE -> {
                FromViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_from, parent, false))
            }
            else -> {
                ToViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_to,
                        parent,
                        false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        getItem(position)?.let { message ->
            holder.itemView.setOnLongClickListener {
                onLongPress(message)
                true
            }
            holder.bind(message, deleteAttachmentListener)
        }

    }

    override fun getItemId(position: Int): Long {
        return getItem(position)?.id?.toLong() ?: -1
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)?.userId) {
            1 -> TO_VIEW_TYPE
            else -> FROM_VIEW_TYPE
        }
    }

    abstract class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        open fun bind(
            message: Message,
            deleteAttachmentListener: (Message, Attachment) -> Unit
        ) {
            itemView.text_message.text = message.content
            itemView.recycler_attachments.layoutManager = LinearLayoutManager(itemView.context)
            itemView.recycler_attachments.isNestedScrollingEnabled = false
            itemView.recycler_attachments.adapter =
                AttachmentsAdapter(message, deleteAttachmentListener)
        }
    }

    class FromViewHolder(view: View) : MessageViewHolder(view) {

        override fun bind(
            message: Message,
            deleteAttachmentListener: (Message, Attachment) -> Unit
        ) {
            super.bind(message, deleteAttachmentListener)
            itemView.text_username.text = message.user.name
            Picasso.get()
                .load(message.user.avatarId)
                .transform(CircleTransform())
                .into(itemView.image_avatar)
        }
    }

    class ToViewHolder(view: View) : MessageViewHolder(view)
}
