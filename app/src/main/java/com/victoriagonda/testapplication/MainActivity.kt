package com.victoriagonda.testapplication

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val viewModelFactory: ViewModelProvider.Factory by inject<MessagesViewModelFactory>()
    private lateinit var viewModel: MessagesViewModel
    private val messagesAdapter = MessagesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpRecyclerView()
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MessagesViewModel::class.java)

        viewModel.observeMessages().observe(this, Observer {
            messagesAdapter.submitList(it)
        })
        viewModel.fetchMessages()
    }

    private fun setUpRecyclerView() {
        recycler_messages.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            messagesAdapter.onLongPress = { message ->
                AlertDialog.Builder(this@MainActivity)
                    .setTitle(R.string.dialog_title_delete_message)
                    .setMessage(
                        getString(R.string.dialog_message_delete_message, message.user.name))
                    .setPositiveButton(R.string.dialog_positive_delete_message) { _, _ ->
                        viewModel.deleteMessage(message)
                    }
                    .setNegativeButton(R.string.dialog_negative_delete_message, null)
                    .show()
            }
            messagesAdapter.deleteAttachmentListener = { message, attachment ->
                AlertDialog.Builder(this@MainActivity)
                    .setTitle(R.string.dialog_title_delete_attachment)
                    .setMessage(
                        getString(R.string.dialog_message_delete_attachment))
                    .setPositiveButton(R.string.dialog_positive_delete_attachment) { _, _ ->
                        viewModel.deleteAttachment(message, attachment)
                    }
                    .setNegativeButton(R.string.dialog_negative_delete_attachment, null)
                    .show()
            }
            adapter = messagesAdapter
        }
    }
}
