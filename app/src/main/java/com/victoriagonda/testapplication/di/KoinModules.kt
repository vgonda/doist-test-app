package com.victoriagonda.testapplication.di

import androidx.room.Room
import com.google.gson.Gson
import com.victoriagonda.testapplication.MessagesViewModelFactory
import com.victoriagonda.testapplication.cache.CacheStore
import com.victoriagonda.testapplication.cache.db.AppDatabase
import com.victoriagonda.testapplication.data.MessagesRepository
import com.victoriagonda.testapplication.data.interactors.DeleteAttachmentFromMessage
import com.victoriagonda.testapplication.data.interactors.DeleteMessage
import com.victoriagonda.testapplication.data.interactors.FetchMessagesIfNeeded
import com.victoriagonda.testapplication.data.interactors.GetMessages
import com.victoriagonda.testapplication.data.mapper.AttachmentCacheMapper
import com.victoriagonda.testapplication.data.mapper.MessageCacheMapper
import com.victoriagonda.testapplication.data.mapper.UserCacheMapper
import com.victoriagonda.testapplication.remote.RemoteStore
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module {

    single { RemoteStore(get(), Gson()) }

    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "app-database.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    single { CacheStore(get()) }

    single { UserCacheMapper() }

    single { AttachmentCacheMapper() }

    single { MessageCacheMapper(get(), get()) }

    single { MessagesRepository(get(), get(), get()) }

    single { GetMessages(get(), Schedulers.io(), AndroidSchedulers.mainThread()) }

    single { DeleteMessage(get(), Schedulers.io(), AndroidSchedulers.mainThread()) }

    single { FetchMessagesIfNeeded(get(), Schedulers.io(), AndroidSchedulers.mainThread()) }

    single { DeleteAttachmentFromMessage(get(), Schedulers.io(), AndroidSchedulers.mainThread()) }

    single { MessagesViewModelFactory(get(), get(), get(), get(), androidApplication()) }
}